﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercice1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int x;
            Console.WriteLine("Veuillez choisir un nombre entier qui sera le dernier nombre de la liste:");
            var listeDeNombre = new List<int>();
            try
            {
                listeDeNombre = MakeArray(Int32.Parse(Console.ReadLine()));
                var listeNombrePremier = FirstList(listeDeNombre);
                listeNombrePremier.ForEach(f =>
                {
                    Console.WriteLine(f);
                });
            }
            catch
            {
                Console.WriteLine("Non c'est pas bon ! On a dit un entier!");
            }
        }

        public static List<int> MakeArray(int nb)
        {
            var listeDeNombre = new List<int>();

            for (int i = 1; i <= nb; i++)
            {
                listeDeNombre.Add(i);
            }

            return listeDeNombre;
        }

        public static List<int> FirstList(List<int> list)
        {
            var nombrePremier = (from entier in list
                          where Premier(entier)
                          select entier).ToList();

            return nombrePremier;
        }

        public static int NombreFacteur(int N)
        {
            int Count = 2, I;
            double Racine = Math.Sqrt(N);
            for (I = 2; I <= Racine; I++) if (N % I == 0) Count++;
            return Count;
        }
        public static bool Premier(int N)
        {
            return (NombreFacteur(N) == 2);
        }
        public static int NombrePremierPlusPetit(int N)
        {
            int Count = 0;
            for (int I = 2; I < N; I++) if (Premier(I)) Count++;
            Console.ReadLine();
            return Count;
        }
    
}
}
