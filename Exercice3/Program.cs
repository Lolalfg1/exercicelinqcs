﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercice3
{
    class Program
    {
        static void Main(string[] args)
        {
            var listeDeNombre = new List<int>();
            Console.WriteLine("Veuillez choisir un entier divible par 5, le nombre choisit sera le dernier de la liste");
            try
            {
                listeDeNombre = MakeArray(Int32.Parse(Console.ReadLine()));
                var list = listeDeNombre.Where(x => x % 5 == 0).ToList();
                list.ForEach(x =>
                {
                    Console.WriteLine(x);
                });
            }
            catch
            {
                Console.WriteLine("Non c'est pas bon ! On a dit un entier!");
            }


        }

        public static List<int> MakeArray(int nb)
        {
            var listeDeNombre = new List<int>();

            for (int i = 0; i <= nb; i++)
            {
                listeDeNombre.Add(i);
            }

            return listeDeNombre;
        }
    
    }
}
