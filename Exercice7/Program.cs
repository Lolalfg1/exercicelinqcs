﻿using Exercice7.Structure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercice7
{
    class Program
    {
        static void Main(string[] args)
        {
            var listeArticle = new List<Article>
            {
                new Article("Jean", 12, 1),
                new Article("T-shirt", 5, 4),
                new Article("Chaussure", 30, 1),
                new Article("Manteau", 50, 1)
            };


            Console.WriteLine($"Quantité Minimum = {listeArticle.Min(a => a.Quantite)}");
            Console.WriteLine($"Quantité Maximum = {listeArticle.Max(a => a.Quantite)}");
            Console.WriteLine($"Prix Moyen = {listeArticle.Average(a => a.Prix)}");
        }
    }
}
