﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercice7.Structure
{
    public class Article
    {

        public string Nom { get; set; }
        public double Prix { get; set; }
        public int Quantite { get; set; }

        public Article(string nom, double prix, int quantite)
        {
            Nom = nom;
            Prix = prix;
            Quantite = quantite;
        }

    }
}
