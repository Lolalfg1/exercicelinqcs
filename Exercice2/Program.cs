﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercice2
{
    class Program
    {
        static void Main(string[] args)
        {
            var listeAnne = new List<int>()
            { 1996, 1998, 2000, 2003, 2016, 2020};

            var requete = from annee in listeAnne
                          where anneeBissextile(annee)
                          select annee;
            var listeDate = requete.ToList();
            listeDate.ForEach(x =>
            {
                Console.WriteLine(x);
            });
        }

        public static bool anneeBissextile(int annee)
        {
            var date1 = new DateTime(annee, 1, 1);
            var date2 = new DateTime(annee, 12, 31);

            TimeSpan ts = date2 - date1;

            var nombreJour = ts.TotalDays + 1;
            if (nombreJour == 366)
                return true;
            return false;
        }
    }
}

