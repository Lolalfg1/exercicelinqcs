﻿using Exercice5.Structure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercice5
{
    class Program
    {
        static void Main(string[] args)
        {
            var listeArticle = new List<Article>
            {
                new Article("Jean", 12, 1),
                new Article("T-shirt", 5, 4),
                new Article("Chaussure", 30, 1),
                new Article("Manteau", 50, 1)
            };

            Console.Write("Saisissez le nom de l'article à afficher : ");
            var nom = Console.ReadLine();

            var article = listeArticle.FirstOrDefault(a => a.Nom == nom);

            if (article != null)
            {
                Console.WriteLine($"Nom: {article.Nom}, Prix: {article.Prix}, Quantité: {article.Quantite}");
            }
            else
            {
                Console.WriteLine("Aucun article ne correspond à votre demande");
            }
        }
    }
}
