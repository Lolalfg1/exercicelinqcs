﻿using Exercice6.Structure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercice6
{
    class Program
    {
        static void Main(string[] args)
        {


            var listeArticle = new List<Article>
            {
                new Article("Jean", 12, 1),
                new Article("T-shirt", 5, 4),
                new Article("Chaussure", 30, 1),
                new Article("Manteau", 50, 1)
            };

            Console.Write("Saisissez le nom de l'article à afficher : ");
            var nom = Console.ReadLine();

            var article = RechercheArticle(listeArticle, nom);
            Console.WriteLine($"Nom: {nom}, Prix: {article.Item1}, Quantité: {article.Item2}");
        }

        public static Tuple<double, int> RechercheArticle(List<Article> list, string nom)
        {
            var article = list.FirstOrDefault(a => a.Nom == nom);
            return new Tuple<double, int>(article.Prix, article.Quantite);
        }
    }
}
