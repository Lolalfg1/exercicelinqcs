﻿using Exercice4.Structure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercice4
{
    class Program
    {
        static void Main(string[] args)
        {
            var listeArticle = new List<Article>
            {
                new Article("Jean", 12, 1),
                new Article("T-shirt", 5, 4),
                new Article("Chaussure", 30, 1),
                new Article("Manteau", 50, 1)
            };

            var arts = listeArticle.Where(a => a.Quantite == 1).ToList();

            arts.ForEach(art =>
            {
                Console.WriteLine($"Nom : {art.Nom}, Prix : {art.Prix}, Quantite : {art.Quantite}");
            });
        
    }
    }
}
